#!/usr/bin/env bash
    
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


docker run --rm -d \
    --env="MYSQL_ROOT_PASSWORD=cromwell" \
    --env="MYSQL_DATABASE=cromwell_db" \
    -p 3306:3306 \
    -v $SCRIPTDIR/cromwell-mysql-local/init:/docker-entrypoint-initdb.d \
    -v $SCRIPTDIR/cromwell-mysql-local/data:/var/lib/mysql mysql:5.7
