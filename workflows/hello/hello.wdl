task hello {
  String name

  command {
    echo 'Hello ${name}!'
  }
  output {
    File response = stdout()
  }
}

workflow test {

  String unmapped_bam = "/some/path/to/input.bam"
  String sub_strip_path = "/.*/"
  String sub_sub = sub(sub(unmapped_bam, sub_strip_path, ""), "bam", "")

  call hello {
    input:
      name = sub_sub
  }
}
