#!/bin/bash

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

set -o errexit
set -o nounset

# The cromwell conf
CROMWELL_CONF=$(readlink -e $1)

# destination bucket for workspace and output locations
OUTPUT_DIR=$(readlink -m $2)

# google mysql instance connection name
# assumes sql proxy is used or db is local
INSTANCE_CONNECTION_NAME=$3

mkdir -p $OUTPUT_DIR

if [ -n "$INSTANCE_CONNECTION_NAME" ] && [ "$INSTANCE_CONNECTION_NAME" != "localdb" ]; then
    $SCRIPTDIR/cloud_sql_proxy -instances=${INSTANCE_CONNECTION_NAME}=tcp:3306 &
fi

cd $OUTPUT_DIR
java -jar -Dconfig.file=$CROMWELL_CONF $SCRIPTDIR/cromwell/cromwell.jar server

#docker run -ti --rm -p 8000:8000 \
#    -v=$CROMWELL_CONF:/cromwell/application.conf \
#    -v=$OUTPUT_DIR:/wdl_runner_local \
#    --network host --workdir /wdl_runner_local \
#    gcr.io/caddy-testing/cromwell_image:0.1.1 \
#    java -jar -Dconfig.file=/cromwell/application.conf /cromwell/cromwell.jar server

