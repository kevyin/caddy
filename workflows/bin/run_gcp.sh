#!/bin/bash

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

set -o errexit
set -o nounset

WOMTOOL_JAR=$SCRIPTDIR/womtool/womtool.jar

# The cromwell conf
# By default it's the Job Execution Service / Google Pipelines API
# but can be modified to run jobs with other engines
# Note this is a template and variables get replaced in wdl_runner.py
JES_TEMPLATE_CONF=$1

# Directory containing the wdl, input.json, options.json
CONFIG_DIR=$2

# destination bucket for workspace and output locations
BUCKET=$3

INSTANCE_CONNECTION_NAME=$4

RUNNAME=${5:-my-path}

OPTIONS=`ls $CONFIG_DIR/*.options.json`
WDL=`ls $CONFIG_DIR/*.wdl`
INPUTS=`ls $CONFIG_DIR/*inputs.json`

java -jar $WOMTOOL_JAR validate $WDL

echo gcloud alpha genomics pipelines run \
  --pipeline-file run_gcp.yaml \
  --zones us-east1-c \
  --logging gs://$BUCKET/$RUNNAME/logging \
  --inputs-from-file JES_TEMPLATE_CONF=$JES_TEMPLATE_CONF \
  --inputs-from-file WDL=$WDL \
  --inputs-from-file WORKFLOW_INPUTS=$INPUTS \
  --inputs-from-file WORKFLOW_OPTIONS=$OPTIONS \
  --inputs WORKSPACE=gs://$BUCKET/$RUNNAME/workspace \
  --inputs OUTPUTS=gs://$BUCKET/$RUNNAME/outputs \
  --inputs INSTANCE_CONNECTION_NAME=$INSTANCE_CONNECTION_NAME \
  --service-account-scopes https://www.googleapis.com/auth/sqlservice.admin

