#!/usr/bin/env bash
    
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

set -o errexit
set -o nounset

CROMWELL_SERVER=${1:-localhost:8000}


curl -X GET "http://${CROMWELL_SERVER}/api/workflows/v1/query?status=Running" -H  "accept: application/json"
