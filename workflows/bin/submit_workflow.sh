#!/usr/bin/env bash
    
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

set -o errexit
set -o nounset

WOMTOOL_JAR=$SCRIPTDIR/womtool/womtool.jar

# Directory containing the wdl, input.json, options.json
CONFIG_DIR=$(readlink -e $1)
INPUTS=$(readlink -e $2)

CROMWELL_SERVER=${3:-localhost:8000}

OPTIONS=`ls $CONFIG_DIR/*.options.json`
WDL=`ls $CONFIG_DIR/*.wdl`
DEFAULT_INPUTS=`ls $CONFIG_DIR/*default.inputs.json`

java -jar $WOMTOOL_JAR validate $WDL

curl -X POST --header "Accept: application/json" -v "http://${CROMWELL_SERVER}/api/workflows/v1" \
	-F workflowSource=@${WDL} \
	-F workflowInputs=@${DEFAULT_INPUTS} \
	-F workflowInputs_2=@${INPUTS} \
	-F workflowOptions=@${OPTIONS}
