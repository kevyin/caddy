
all: workflows/bin/womtool/womtool.jar workflows/bin/cromwell/cromwell.jar

workflows/bin/cromwell/cromwell-30.2.jar:
	mkdir -p workflows/bin/cromwell/ && cd workflows/bin/cromwell/ && wget https://github.com/broadinstitute/cromwell/releases/download/30.2/cromwell-30.2.jar

workflows/bin/womtool/womtool-30.2.jar:
	mkdir -p workflows/bin/womtool/ && cd workflows/bin/womtool/ && wget https://github.com/broadinstitute/cromwell/releases/download/30.2/womtool-30.2.jar

workflows/bin/womtool/womtool.jar:
	cd workflows/bin/womtool/ && ln -s womtool-30.2.jar womtool.jar

workflows/bin/cromwell/cromwell.jar:
	cd workflows/bin/cromwell/ && ln -s cromwell-30.2.jar cromwell.jar
