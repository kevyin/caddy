# Caddy

wdl pipelines


## Running

    ## Start the mysql
    # as localdb, same machine as server
    bash start_localdb.sh

    ## cromwell server
    # local config, ie running on single machine
    bash workflows/bin/server.sh ./cromwell_configs/local_localdb.conf OUTDIR/ localdb 2>&1 | tee /tmp/cromwell_server.log

    # gcp config
    bash workflows/bin/server.sh ./cromwell_configs/gcp_localdb.conf OUTDIR/ localdb 2>&1 | tee /tmp/cromwell_server.log

    ## submit job
    # local test
    bash workflows/bin/submit_workflow.sh \
        workflows/workflows/ubam-gvcf_broad-gatk_local/ \
        test/inputs/test.na12878-*.inputs.json \
        localhost:8000

    # gcp test
    bash workflows/bin/submit_workflow.sh \
        workflows/workflows/ubam-gvcf_broad-gatk_gcp/ \
        test/inputs/test.na12878-gcp.inputs.json \
        localhost:8000


    ## Monitor

    # running jobs
    bash workflows/bin/get_workflow_running.sh localhost:8000 | python -m json.tool

    # via web ui
    gcloud compute firewall-rules create GCP_VM_INSTANCE_NAME --allow tcp:8000
    http://IPADDRESS:8000/api/workflows/v1/WORKFLOWID/timing

