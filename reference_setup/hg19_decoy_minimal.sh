#!/usr/bin/env bash
    
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

set -o errexit
set -o nounset

DEST=${1:-./}

# setup a hg19 decoy reference from
# gs://broad-references/Homo_sapiens_assembly19_1000genomes_decoy
# gs://broad-references/hg19/v0
gsutil ls gs://broad-public-datasets/NA12878_downsampled_for_testing/unmapped/ \
    | grep -f $SCRIPTDIR/hg19_decoy.list | parallel --gnu "gsutil cp {} $DEST"

gsutil ls gs://broad-references/Homo_sapiens_assembly19_1000genomes_decoy \
    | grep -f $SCRIPTDIR/hg19_decoy.list | parallel --gnu "gsutil cp {} $DEST"

gsutil ls gs://broad-references/hg19/v0 \
    | grep -f $SCRIPTDIR/hg19_decoy.list | parallel --gnu "gsutil cp {} $DEST"

bgzip Homo_sapiens_assembly19_1000genomes_decoy.known_indels.vcf
tabix Homo_sapiens_assembly19_1000genomes_decoy.known_indels.vcf.gz