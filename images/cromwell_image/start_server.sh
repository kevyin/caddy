#!/usr/bin/env bash

java -jar -Xmx4g -Dconfig.file=/cromwell/application.conf \
    /cromwell/cromwell.jar server 2>&1 | tee -a /var/log/cromwell_server.log